//
//  ChangeHolidayTypeTableViewCell.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/29/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class ChangeHolidayTypeTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var checkImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
