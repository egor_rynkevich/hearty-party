//
//  DietDN.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/29/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class DietDN: ASDisplayNode {

    var tableNode: ASTableNode = ASTableNode(style: .plain)
    var dietArr: [DietEnum] = []
    var choosedArr: [DietEnum] = []
    
    override init() {
        super.init()
        tableNode.dataSource = self
        tableNode.delegate = self
        tableNode.view.isScrollEnabled = false
        tableNode.view.bounces = true
        tableNode.view.separatorStyle = .none
        //tableNode.backgroundColor = UIColor.red
        tableNode.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30000)
        
        for val in DietEnum.allCases {
            dietArr.append(val)
        }
        
        tableNode.reloadData()
        tableNode.waitUntilAllUpdatesAreProcessed()
        addSubnode(tableNode)
        
    }
    
    override func nodeDidLoad() {
        super.nodeDidLoad()
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: tableNode)
    }
}

extension DietDN: ASTableDelegate, ASTableDataSource {
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return 1
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return dietArr.count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = DietCellNode(title: "\(self.dietArr[indexPath.row])")
            return node
        }
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        
        let cell = tableNode.nodeForRow(at: indexPath) as? DietCellNode
        cell?.changeCellState()
        
        if let ind = choosedArr.firstIndex(of: dietArr[indexPath.row]) {
            choosedArr.remove(at: ind)
        } else {
            choosedArr.append(dietArr[indexPath.row])
        }
    }
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }
}
