//
//  ChangeFiltersViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/29/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class ChangeFiltersViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    var holiday: ChangeHolidayTypeView?
    var theme: ChangeThemeView?
    var holidayType: HolidayTypeEnum = .BBQ
    var themeArr: [DishNationEnum] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        scrollView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        self.navigationController?.navigationBar.prefersLargeTitles = false

        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        btn.setTitle("Back", for: .normal)
        btn.setTitleColor(UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1), for: .normal)
        btn.setImage(UIImage(named: "Back")!.resize(targetSize: CGSize(width: 12, height: 20)).withRenderingMode(.alwaysTemplate), for: .normal)
        btn.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 6)
        btn.addTarget(self, action: #selector(backBtn), for: .touchUpInside)
        
        let back = UIBarButtonItem(customView: btn)
        self.navigationItem.setLeftBarButton(back, animated: true)
        
        holiday = ChangeHolidayTypeView(frame: CGRect(x: 0, y: 37, width: UIScreen.main.bounds.width, height: 750))
        holiday?.holidayType = holidayType
        holiday?.tableView.reloadData()
        
        scrollView.addSubview(holiday!)
        
        theme = ChangeThemeView(frame: CGRect(x: 0, y: 805, width: UIScreen.main.bounds.width, height: 355))
        theme?.theme = themeArr
        theme?.tableView.reloadData()
        
        scrollView.addSubview(theme!)
        
        scrollView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1160)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1160)
    }
    
    @objc func backBtn() {
        self.navigationController?.popViewController(animated: true)
    }
}
