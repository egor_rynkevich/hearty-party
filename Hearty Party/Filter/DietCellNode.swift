//
//  DietCellNode.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/29/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class DietCellNode: ASCellNode {
    fileprivate let titleTextNode: ASTextNode
    fileprivate let imageNode: ASImageNode
    var isSelect = false
    var title = ""
    
    init(title: String) {
        titleTextNode = ASTextNode()
        imageNode = ASImageNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        let backView = UIView()
        backView.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
        selectedBackgroundView = backView
        
        var str = title
        str.seperatedWithSpaces()
        self.title = str
        let attributedString = NSMutableAttributedString(string: str, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
        
        titleTextNode.attributedText = attributedString
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: UIScreen.main.bounds.width - 70)
        imageNode.image = UIImage(named: "unselect")?.resize(targetSize: CGSize(width: 22, height: 22))
        imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1))
        addSubnode(titleTextNode)
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let stack = ASStackLayoutSpec(direction: .horizontal, spacing: 12, justifyContent: .start, alignItems: .center, children: [imageNode, titleTextNode])
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 11, left: 16, bottom: 11, right: 15), child: stack)
    }
    
    func changeCellState() {
        var imageName = ""
        
        if isSelect {
            imageName = "unselect"
            isSelect = false
        } else {
            imageName = "select"
            isSelect = true
        }
        
        imageNode.image = UIImage(named: imageName)?.resize(targetSize: CGSize(width: 22, height: 22))
    }
}
