//
//  ChangeThemeView.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/29/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class ChangeThemeView: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    var arr: [DishNationEnum] = []
    var theme: [DishNationEnum] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ChangeThemeView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //view.backgroundColor = .red
        registerNib()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
        tableView.bounces = true
        tableView.separatorStyle = .none
        
        for val in DishNationEnum.allCases { arr.append(val) }
        tableView.reloadData()
    }
    
    func registerNib() {
        let nib = UINib(nibName: "ChangeThemeTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ChangeThemeTableViewCell")
    }

}

extension ChangeThemeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeThemeTableViewCell") as? ChangeThemeTableViewCell {
            if indexPath.row == 0 {
                cell.lbl.text = "None"
                cell.imgView.image = UIImage(named: "None")?.resize(targetSize: CGSize(width: 22, height: 22))
                if theme.count == 0 {
                    cell.checkImgView.image = UIImage(named: "checkmark")
                    cell.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
                }
            } else {
                var str = "\(arr[indexPath.row - 1])"
                str.seperatedWithSpaces()
                
                cell.lbl.text = str
                cell.imgView.image = UIImage(named: "\(arr[indexPath.row - 1])")?.resize(targetSize: CGSize(width: 22, height: 22))
                
                
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    
}
