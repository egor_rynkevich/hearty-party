//
//  ChangeHolidayTypeView.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/29/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class ChangeHolidayTypeView: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    var arr: [HolidayTypeEnum] = []
    var holidayType: HolidayTypeEnum = .BBQ
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ChangeHolidayTypeView", owner: self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //view.backgroundColor = .red
        registerNib()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
        tableView.bounces = true
        tableView.separatorStyle = .none
        for val in HolidayTypeEnum.allCases { arr.append(val) }
        tableView.reloadData()
    }
    
    func registerNib() {
        let nib = UINib(nibName: "ChangeHolidayTypeTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ChangeHolidayTypeTableViewCell")
    }

}

extension ChangeHolidayTypeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeHolidayTypeTableViewCell") as? ChangeHolidayTypeTableViewCell {
            var str = "\(arr[indexPath.row])"
            str.seperatedWithSpaces()
            cell.lbl.text = str
            
            cell.imgView.image = UIImage(named: "\(arr[indexPath.row])")?.resize(targetSize: CGSize(width: 22, height: 22))
            
            if holidayType.rawValue == arr[indexPath.row].rawValue {
                cell.checkImgView.image = UIImage(named: "checkmark")
                cell.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    
}
