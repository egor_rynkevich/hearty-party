//
//  FiltersViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/28/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit

protocol FiltersViewControllerDelegate: class{
    func setNewData(num: Int, diet: [DietEnum])
}

class FiltersViewController: UIViewController {

    var scrollNode: ASScrollNode = ASScrollNode()
    
    var numGuests: Int = 0
    var holidayType: HolidayTypeEnum = .BBQ
    var themeArr: [DishNationEnum] = []
    var dishTypeArr: [DishTypeEnum] = []
    var dietDN: DietDN?
    var pickerNode: ASDisplayNode?
    var delegate:FiltersViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Settings"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        scrollNode.automaticallyManagesSubnodes = true
        scrollNode.automaticallyManagesContentSize = true
        scrollNode.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        
        pickerNode = ASDisplayNode { () -> UIView in
            let picker = UIPickerView()
            //picker.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
            picker.backgroundColor = UIColor.white
            picker.dataSource = self
            picker.delegate = self
            picker.selectRow(self.numGuests - 1, inComponent: 0, animated: true)
            return picker
        }
        pickerNode?.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 180)
        
        dietDN = DietDN()
        
        var size = dietDN?.tableNode.view.contentSize
        size?.height += 10
        dietDN?.style.preferredSize = size!
        
        scrollNode.layoutSpecBlock = { node, constrainedSize in
            let stack = ASStackLayoutSpec.vertical()
            
            let line1 = ASDisplayNode()
            line1.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line1.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 36, left: 0, bottom: 0, right: 0), child: line1))
            
            let imgNode = ASImageNode()
            let ocasTitleTextNode = ASTextNode()
            let themeTextNode = ASTextNode()
            let arrowImageNode = ASImageNode()
            
            imgNode.image = UIImage(named: "\(self.holidayType)")?.resize(targetSize: CGSize(width: 60, height: 60))
            imgNode.addTarget(self, action: #selector(self.openHolThemeNumView), forControlEvents: .touchUpInside)
            ocasTitleTextNode.attributedText = NSAttributedString(string: "\(self.holidayType)", attributes: [.font : UIFont.boldSystemFont(ofSize: 19), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            var str = self.themeArr.count == 0 ? "Without theme" : "\(self.themeArr[0])"
            ocasTitleTextNode.style.minWidth = ASDimension(unit: .points, value: UIScreen.main.bounds.width - 141)
            str.seperatedWithSpaces()
            themeTextNode.attributedText = NSAttributedString(string: str, attributes: [.font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1)])
            
            arrowImageNode.image = UIImage(named: "arrow_right")?.resize(targetSize: CGSize(width: 8, height: 13))
            arrowImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor(red: 0.82, green: 0.82, blue: 0.84, alpha: 1))
            
            let ocasStack = ASStackLayoutSpec(direction: .vertical, spacing: 5, justifyContent: .start, alignItems: .start, children: [ocasTitleTextNode, themeTextNode])
            
            let resStack = ASStackLayoutSpec(direction: .horizontal, spacing: 15, justifyContent: .start, alignItems: .center, children: [imgNode, ocasStack, arrowImageNode])
            
            let backNode = ASDisplayNode()
            backNode.backgroundColor = .white
            
            
            let backL = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 9, left: 15, bottom: 9, right: 15), child: resStack), background: backNode)
            
            stack.children?.append(backL)
            
            let line2 = ASDisplayNode()
            line2.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line2.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: line2))
            
            let line3 = ASDisplayNode()
            line3.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line3.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0), child: line3))
            
            let numTextNode = ASTextNode()
            numTextNode.attributedText = NSAttributedString(string: "Guests", attributes: [.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            let back3Node = ASDisplayNode()
            back3Node.backgroundColor = .white
            
            let back3L = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 11, left: 16, bottom: 0, right: 15), child: numTextNode), background: back3Node)
            stack.children?.append(back3L)
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: self.pickerNode!))
            
            let line7 = ASDisplayNode()
            line7.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line7.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: line7))
            
            let line5 = ASDisplayNode()
            line5.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line5.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0), child: line5))
            
            let dietTextNode = ASTextNode()
            dietTextNode.attributedText = NSAttributedString(string: "Diet", attributes: [.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            
            let back2Node = ASDisplayNode()
            back2Node.backgroundColor = .white
            
            let back2L = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 11, left: 16, bottom: 8, right: 15), child: dietTextNode), background: back2Node)
            
            stack.children?.append(back2L)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: self.dietDN!))
            
            let line8 = ASDisplayNode()
            line8.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line8.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: line8))
            
            let line6 = ASDisplayNode()
            line6.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line6.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0), child: line6))
            
            let excludedTextNode = ASTextNode()
            excludedTextNode.attributedText = NSAttributedString(string: "Excluded products", attributes: [.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            
            let addBtn = ASButtonNode()
            
            addBtn.setTitle("ADD", with: UIFont.boldSystemFont(ofSize: 17), with: UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1), for: .normal)
            addBtn.contentEdgeInsets = UIEdgeInsets(top: 5, left: 14, bottom: 5, right: 14)
            addBtn.cornerRadius = 14
            addBtn.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.97, alpha: 1)
            
            let exStack = ASStackLayoutSpec(direction: .horizontal, spacing: 1, justifyContent: .spaceBetween, alignItems: .center, children: [excludedTextNode, addBtn])
            
            let back4Node = ASDisplayNode()
            back4Node.backgroundColor = .white
            
            let back4L = ASBackgroundLayoutSpec(child: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 16, bottom: 80, right: 15), child: exStack), background: back4Node)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: back4L))
            
            return stack
        }
        
        self.view.addSubnode(scrollNode)
        
        
        let shareBar: UIBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveBtn))
        shareBar.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        self.navigationItem.rightBarButtonItem = shareBar
        
        let back = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backBtn))
        back.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        self.navigationItem.setLeftBarButton(back, animated: true)
        
        
    }
    
    @objc func saveBtn() {
        
        delegate?.setNewData(num: numGuests, diet: dietDN?.choosedArr ?? [])
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func backBtn() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.scrollNode.frame = self.view.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func openHolThemeNumView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "ChangeFiltersViewController") as? ChangeFiltersViewController {
            //let myIndexPath = self.tableNode.indexPathForSelectedRow!
            //cntlr.dishTypeArr = dishTypeArr
            cntlr.holidayType = holidayType
            cntlr.themeArr = themeArr
            //cntlr.numGuests = numGuests
            
            self.navigationController?.pushViewController(cntlr, animated: true)
        }
    }
    
}

extension FiltersViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 50
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row+1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let myTitle = NSAttributedString(string: "\(row+1)", attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedString.Key.foregroundColor:UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1)])
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        numGuests = row + 1
        //numberLbl.text = "\(numGuests)"
    }
}

extension UIBarButtonItem {
    convenience init(image :UIImage, title :String, target: Any?, action: Selector?) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setTitle(title, for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        
        if let target = target, let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        
        self.init(customView: button)
    }
}
