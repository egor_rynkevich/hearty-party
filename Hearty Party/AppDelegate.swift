//
//  AppDelegate.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/17/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import RealmSwift
import AsyncDisplayKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 3,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        Realm.Configuration.defaultConfiguration = config
        print(Realm.Configuration.defaultConfiguration.fileURL)
//        let realmURL = Realm.Configuration.defaultConfiguration.fileURL ?? URL(string: "")
//        let realmURLs = [
//            realmURL,
//            realmURL?.appendingPathExtension("lock"),
//            realmURL?.appendingPathExtension("note"),
//            realmURL?.appendingPathExtension("management")
//        ]
//        for URL in realmURLs {
//            do {
//                try FileManager.default.removeItem(at: URL!)
//            } catch {
//                // handle error
//            }
//        }
        //let config = Realm.Configuration(fileURL: Bundle.main.url(forResource: "RealmDbTest", withExtension: "realm"), readOnly: false)
        //Realm.Configuration.defaultConfiguration
        //print(Realm.Configuration.defaultConfiguration.fileURL!)
        // Open the Realm with the configuration
        //let _ = try! Realm(configuration: config)
        // Override point for customization after application launch.
        
        
        let colorView = UIView()
        colorView.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
        
        
        
        // use UITableViewCell.appearance() to configure
        // the default appearance of all UITableViewCells in your app
        UITableViewCell.appearance().selectedBackgroundView = colorView
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

