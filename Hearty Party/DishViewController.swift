//
//  DishViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/27/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class DishViewController: UIViewController {

    var scrollNode: ASScrollNode = ASScrollNode()
    var dish: Dish?
    var peopleCount: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        title = "Recipe"
        
        let shareBar: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareBtn))
        shareBar.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        self.navigationItem.rightBarButtonItem = shareBar
    
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        btn.setTitle("Back", for: .normal)
        btn.setTitleColor(UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1), for: .normal)
        btn.setImage(UIImage(named: "Back")!.resize(targetSize: CGSize(width: 12, height: 20)).withRenderingMode(.alwaysTemplate), for: .normal)
        btn.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 6)
        btn.addTarget(self, action: #selector(backBtn), for: .touchUpInside)
        
        
        let back = UIBarButtonItem(customView: btn)
        self.navigationItem.setLeftBarButton(back, animated: true)
        
        let imageNode = ASNetworkImageNode()
        imageNode.url = NSURL(string: dish!.PosterUrl!)! as URL
        imageNode.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 211)
        //scrollNode.addSubnode(imageNode)
        scrollNode.automaticallyManagesSubnodes = true
        scrollNode.automaticallyManagesContentSize = true
        //scrollNode.backgroundColor = UIColor.blue
        //scrollNode.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        scrollNode.layoutSpecBlock = { node, constrainedSize in
            let stack = ASStackLayoutSpec.vertical()
            stack.children?.append(imageNode)
            
            let titleTextNode = ASTextNode()
            titleTextNode.attributedText = NSAttributedString(string: self.dish!.Name!, attributes: [.font : UIFont.systemFont(ofSize: 24), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 17, left: 21, bottom: 0, right: 13), child: titleTextNode))
            
            let textOfSize: CGFloat = 17
            
            let peopleImageNode = ASImageNode()
            peopleImageNode.image = UIImage(named: "people.png")?.resize(targetSize: CGSize(width: 22, height: 16))
            peopleImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1))
            let numberTextNode = ASTextNode()
            numberTextNode.attributedText = NSAttributedString(string: "(\(self.peopleCount))", attributes: [.font : UIFont.systemFont(ofSize: textOfSize), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)])
            
            let peopleStck = ASStackLayoutSpec(direction: .horizontal, spacing: 6, justifyContent: .start, alignItems: .center, children: [peopleImageNode, numberTextNode])
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 21, bottom: 0, right: 13), child: peopleStck))
            
            let ingInfos = self.dish!.IngredientInfos
            let ingStack = ASStackLayoutSpec(direction: .vertical, spacing: 5, justifyContent: .start, alignItems: .start, children: [])
            
            for ingInfo in ingInfos {
                var amountTypeStr = ""
                
                if ingInfo.AmountType == IngredientCountTypeEnum.Abstract.rawValue || ingInfo.AmountType == IngredientCountTypeEnum.Whole.rawValue { amountTypeStr = "" }
                else {
                    for type in IngredientCountTypeEnum.allCases {
                        if type.rawValue == ingInfo.AmountType {
                            amountTypeStr = "\(type)"
                            amountTypeStr.seperatedWithSpaces()
                            amountTypeStr = amountTypeStr.lowercased()
                            break
                        }
                    }
                }
                var str = ingInfo.IngredientStrTemplate!
                var amountStr = ""
                
                let res = ingInfo.IgredientAmount / Float(ingInfo.ParentDish!.Serves) * Float(self.peopleCount)
                
                if res < 1 {
                    if res <= 0 { amountStr = "" }
                    else if res > 0 && res <= 0.25 { amountStr = "¼" }
                    else if res > 0.25 && res <= 0.5 { amountStr = "½" }
                    else { amountStr = "¾" }
                } else {
                    amountStr = "\(Int(res))"
                }
                
                
                str = str.replacingOccurrences(of: "{0}", with: amountStr)
                str = str.replacingOccurrences(of: "{1}", with: amountTypeStr)
                str = str.replacingOccurrences(of: "{2}", with: ingInfo.ParentIngredient!.Name!)
                
                let range = (str as NSString).range(of: ingInfo.ParentIngredient!.Name!)
                
                let textNode = ASTextNode()
                let attributedString = NSMutableAttributedString(string: str, attributes: [.font : UIFont.systemFont(ofSize: textOfSize), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1) , range: range)
                textNode.attributedText = attributedString
                textNode.style.maxWidth = ASDimension(unit: .points, value: UIScreen.main.bounds.width - 49)
                ingStack.children?.append(textNode)
            }
            
            let sepNode = ASDisplayNode()
            
            sepNode.backgroundColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
            sepNode.cornerRadius = 1
            sepNode.style.width = ASDimension(unit: .points, value: 2)
            sepNode.style.minHeight = ASDimension(unit: .points, value: 30)
            sepNode.style.flexGrow = 1
            sepNode.style.flexShrink = 1
            
            let hStack = ASStackLayoutSpec(direction: .horizontal, spacing: 13, justifyContent: .start, alignItems: .stretch, children: [ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: -2, bottom: 0, right: 5) , child: sepNode), ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: 0, bottom: 3, right: 0) , child: ingStack)])
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 13, left: 21, bottom: 0, right: 13), child: hStack))
            
            var recipeStr = self.dish!.Instruction!
            
            recipeStr = recipeStr.replacingOccurrences(of: "\n", with: "\n")
            
            var needNumber = 2
            var countEnter = 0
            var index = 0
            recipeStr.insert("1", at: String.Index(encodedOffset: 0))
            recipeStr.insert(".", at: String.Index(encodedOffset: 1))
            recipeStr.insert(" ", at: String.Index(encodedOffset: 2))
            
            for char in recipeStr.characters {
                index += 1
                if char == "\n" {
                    countEnter += 1
                    if countEnter == 2 {
                        let char = Character("\(needNumber)")
                        recipeStr.insert(char, at: String.Index(encodedOffset: index))
                        recipeStr.insert(".", at: String.Index(encodedOffset: index + 1))
                        recipeStr.insert(" ", at: String.Index(encodedOffset: index + 2))
                        index += 3
                        needNumber += 1
                        countEnter = 0
                    }
                }
                
            }
            
            let attributedString = NSMutableAttributedString(string: recipeStr, attributes: [.font : UIFont.systemFont(ofSize: textOfSize), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            
            for i in 1...needNumber {
                let range = (recipeStr as NSString).range(of: "\(i). ")
                
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: textOfSize) , range: range)
            }
            
            let recipeTextNode = ASTextNode()
            recipeTextNode.attributedText = attributedString
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 26, left: 21, bottom: 10, right: 13), child: recipeTextNode))
            
            return stack
        }
        
        self.view.addSubnode(scrollNode)
        
        
        
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.scrollNode.frame = self.view.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func shareBtn() {
        let activity = UIActivityViewController(activityItems: ["text"], applicationActivities: nil)
        activity.popoverPresentationController?.sourceView = self.view
        
        self.present(activity, animated: true, completion: nil)
    }
    
    @objc func backBtn() {
        self.navigationController?.popViewController(animated: true)
    }
}


class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}
