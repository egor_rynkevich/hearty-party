//
//  Models.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/20/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import RealmSwift





enum DishNationEnum: Int, CaseIterable
{
    case Mexican    = 0x01,
    Mediterranean   = 0x02,
    Italian         = 0x04,
    Oriental        = 0x08,
    KidsParty       = 0x10,
    Romantic        = 0x20
}

enum HolidayTypeEnum: Int, CaseIterable
{
    case Dinner          = 0x0001,
    Picnic          = 0x0002,
    Brunch          = 0x0004,
    BBQ             = 0x0008,
    
    Birthday        = 0x0010,
    ChineseNewYear  = 0x0020,
    ValentinesDay   = 0x0040,
    SaintPatrickDay = 0x0080,
    
    MothersDay      = 0x0100,
    Easter          = 0x0200,
    FathersDay      = 0x0400,
    Halloween       = 0x0800,
    
    Thanksgiving    = 0x1000,
    Christmas       = 0x2000,
    Hanukkah        = 0x4000,
    NewYear         = 0x8000
}


enum DietEnum: Int, CaseIterable
{
    case LowCalorie = 0x01,
    Vegetarian = 0x02,
    Meat       = 0x04,
    Fish       = 0x08,
    GlutenFree = 0x10,
    Spicy      = 0x20
}


enum DishTypeEnum: Int
{
    case Appetizer   = 0x01,
    Salad       = 0x02,
    MainCourse  = 0x04,
    Dessert     = 0x08
}

enum IngredientCountTypeEnum: Int, CaseIterable
{
    case Gramm = 1,
    Milliliter,
    Liter,
    TeaSpoon,
    TableSpoon,
    Abstract,   //напр. соль или кетчуп (сколько нужно, по желанию)
    Whole,      //напр. один помидор/киви/банан
    SmallPunnet,// корзинка
    Pinch,      //щепотка
    Slice,
    Slices,
    Handful,    //горсть
    Bunch,
    Piece,
    Packet,
    Drop,
    Dash,        //Черта (типа одна линия масла на сковородку)
    Tin,        //жестяная банка
    Cloves
}

enum IngredientGroup: Int
{
    case FruitAndVegetables = 1,
    CookingIngredients,
    DairyEggsAndChilled,
    MeatFishAndPoultry,
    BiscuitsSnacksAndSweets,
    TinsPacketsAndJars,
    Other
}


class Dish : Object
{
    @objc dynamic var DishId: Int = 0

    //[Backlink(nameof(IngredientInfo.ParentDish))]
    //public IQueryable<IngredientInfo> IngredientInfos = List<IngredientInfo>()
    //let IngredientInfos = List<IngredientInfo>()
    let IngredientInfos = LinkingObjects(fromType: IngredientInfo.self, property: "ParentDish")

    //#region DishData
    @objc dynamic var PosterUrl: String?
    @objc dynamic var Name: String?
    @objc dynamic var PreparationTime: String?
    @objc dynamic var CookingTime: String?
    @objc dynamic var Instruction: String?
    @objc dynamic var Serves: Int = 0
    @objc dynamic var DishPartsNames: String? //напр. Основное блюдо и соус
    @objc dynamic var DishType: Int = 0
    @objc dynamic var DishTheme: Int = 0
    @objc dynamic var HolidayType: Int = 0
    @objc dynamic var Diet: Int = 0
    //#endregion
    
    override static func primaryKey() -> String? {
        return "DishId"
    }
}

class Ingredient : Object
{
    @objc dynamic var IngredientId: Int = 0
    
    //[Backlink(nameof(IngredientInfo.ParentIngredient))]
    let IngredientInfos = LinkingObjects(fromType: IngredientInfo.self, property: "ParentIngredient")
    
    
    //let IngredientInfosList = List<IngredientInfo>() //Это не часть базы. Вспомогательное поле для склейки инфы
    
    //#region IngredientData
    @objc dynamic var Name: String?
    @objc dynamic var GroupId: Int = 0
    //#endregion
    
    override static func primaryKey() -> String? {
        return "IngredientId"
    }
}

class IngredientInfo : Object
{
    @objc dynamic var InfoId: Int = 0
    
    @objc dynamic var ParentIngredient: Ingredient?
    @objc dynamic var ParentDish: Dish?
    
    
    //#region InfoData
    @objc dynamic var IgredientAmount: Float = 0
    @objc dynamic var AmountType: Int = 0
    @objc dynamic var SequenceNumber: Int = 0
    @objc dynamic var DishPart: Int = 0 //напр. Основное блюдо и соус
    @objc dynamic var IngredientStrTemplate: String?
    //#endregion
    
    override static func primaryKey() -> String? {
        return "InfoId"
    }
}


class SavedParty : Object
{
    @objc dynamic var PartyId: Int = 0
    @objc dynamic var PartyCustomName: String?
    @objc dynamic var FilterDishType: Int = 0
    @objc dynamic var FilterNationality: Int = 0
    @objc dynamic var FilterHolidayType: Int = 0
    @objc dynamic var FilterDiet: Int = 0
    let DishesId = List<Int>()
    let Amounts = List<Int>()
    
    override static func primaryKey() -> String? {
        return "PartyId"
    }
}

class SavedPartyIngredients: Object {
    @objc dynamic var SavedPartyIngredientId: Int = 0
    @objc dynamic var PartyId: Int = 0
    let IngredientsId = List<Int>()
    let IsBoughtList = List<Bool>()
    
    override static func primaryKey() -> String? {
        return "SavedPartyIngredientId"
    }
}
