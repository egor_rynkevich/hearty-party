//
//  ThemeCollectionViewCell.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/26/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class ThemeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
}
