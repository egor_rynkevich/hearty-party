//
//  ChooseThemeVC.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/26/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class ChooseThemeVC: UIViewController {

    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    
    var arr: [DishNationEnum] = []
    var choosedThemes: [DishNationEnum] = []
    var numGuests: Int = 1
    var holidayType: HolidayTypeEnum = .BBQ
    var themeArr: [DishNationEnum] = []
    var delegate: ViewControllerDelegate?
    var cellWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for val in DishNationEnum.allCases { arr.append(val) }
        
        cellWidth = (UIScreen.main.bounds.width - 44) / 2
        
        collection.delegate = self
        collection.dataSource = self
        collection.contentInset = UIEdgeInsets(top: 16, left: 15, bottom: 12, right: 16)
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        gradient.startPoint = CGPoint(x: 0.5, y: 0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1)
        gradient.colors = [UIColor(red: 244.0 / 255.0, green: 192.0 / 255.0, blue: 49.0 / 255.0, alpha: 1).cgColor as Any, UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor as Any]
        
        self.view.backgroundColor = UIColor.init(patternImage: UIImage().image(fromLayer: gradient))
        collection.backgroundColor = nil
        
        okBtn.layer.cornerRadius = 27.5
        okBtn.layer.borderWidth = 2
        okBtn.layer.borderColor = UIColor(red: 247.0 / 255.0, green: 193.0 / 255.0, blue: 133.0 / 255.0, alpha: 1).cgColor
        okBtn.setTitleColor(UIColor(red: 247.0 / 255.0, green: 193.0 / 255.0, blue: 133.0 / 255.0, alpha: 1), for: .disabled)
        
        okBtn.addTarget(self, action: #selector(okBtnAction), for: .touchUpInside)
        skipBtn.addTarget(self, action: #selector(skipBtnAction), for: .touchUpInside)
        
        collection.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @objc func okBtnAction() {
        delegate?.generateShopingList(numGuests: self.numGuests, holidayType: self.holidayType, nationArr: self.choosedThemes)
        dismiss(animated: true, completion: nil)
//        dismiss(animated: true) {
//            self.delegate?.generateShopingList(numGuests: self.numGuests, holidayType: self.holidayType, nationArr: self.choosedThemes)
//        }
    }
    
    @objc func skipBtnAction() {
//        dismiss(animated: true) {
//            self.delegate?.generateShopingList(numGuests: self.numGuests, holidayType: self.holidayType, nationArr: [])
//        }
        
        delegate?.generateShopingList(numGuests: self.numGuests, holidayType: self.holidayType, nationArr: self.choosedThemes)
        dismiss(animated: true, completion: nil)
    }

}

extension ChooseThemeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThemeCollectionViewCell", for: indexPath) as? ThemeCollectionViewCell {
            var str = "\(arr[indexPath.row])"
            str.seperatedWithSpaces()
            cell.nameLbl.text = str
            cell.nameLbl.textColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
            cell.imgView.image = UIImage(named: "\(arr[indexPath.row])")
            cell.backgroundColor = UIColor.white
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 2, height: 0)
            cell.layer.shadowRadius = 3
            
            cell.contentView.layer.cornerRadius = 2.0
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 0.5
            cell.layer.masksToBounds = false
            
        return cell
        }
        else { return UICollectionViewCell() }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if choosedThemes.contains(arr[indexPath.row]) {
            choosedThemes.remove(at: indexPath.row)
            if let cell = collection.cellForItem(at: indexPath) as? ThemeCollectionViewCell {
                cell.backgroundColor = UIColor.white
            }
            if choosedThemes.count == 0 {
                okBtn.isEnabled = false
                okBtn.layer.borderColor = UIColor(red: 247.0 / 255.0, green: 193.0 / 255.0, blue: 133.0 / 255.0, alpha: 1).cgColor
                okBtn.setTitleColor(UIColor(red: 247.0 / 255.0, green: 193.0 / 255.0, blue: 133.0 / 255.0, alpha: 1), for: .disabled)
            }
            
        } else {
            if choosedThemes.count == 1 {
                let index = IndexPath(row: arr.firstIndex(where: { (type) -> Bool in
                    return type == choosedThemes[0]
                })!, section: 0)
                choosedThemes.removeAll()
                if let cell = collection.cellForItem(at: index) as? ThemeCollectionViewCell {
                    cell.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }
            
            choosedThemes.append(arr[indexPath.row])

            if let cell = collection.cellForItem(at: indexPath) as? ThemeCollectionViewCell {
                cell.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
            }
            if choosedThemes.count == 1 {
                okBtn.isEnabled = true
                okBtn.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
                okBtn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
}
