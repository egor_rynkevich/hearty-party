//
//  OcassionTableViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/21/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class OcassionTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var arr: [HolidayTypeEnum] = []
    var delegate: ViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        
        for val in HolidayTypeEnum.allCases { arr.append(val) }
        
        self.tableView.reloadData()
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        gradient.startPoint = CGPoint(x: 0.5, y: 0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1)
        gradient.colors = [UIColor(red: 244.0 / 255.0, green: 192.0 / 255.0, blue: 49.0 / 255.0, alpha: 1).cgColor as Any, UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor as Any]
        
        self.view.backgroundColor = UIColor.init(patternImage: UIImage().image(fromLayer: gradient))
        tableView.backgroundColor = nil
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arr.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OcassionTableViewCell", for: indexPath) as? OcassionTableViewCell {
            var str = "\(arr[indexPath.row])"
            str.seperatedWithSpaces()
            cell.configCell(img: UIImage(named: "\(arr[indexPath.row])")?.resize(targetSize: CGSize(width: 40, height: 40)), title: str)

            return cell
        }
        return UITableViewCell()
        
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.dismiss(animated: true, completion: nil)
        delegate?.showNumberVC(holidayType: self.arr[indexPath.row])
    }

}


extension String{
    mutating func seperatedWithSpaces(){
        //indexOffset is needed because each time replaceSubrange is called, the resulting count is incremented by one (owing to the fact that a space is added to every capitalised letter)
        var indexOffset = 0
        for (index, character) in characters.enumerated(){
            let stringCharacter = String(character)
            
            if stringCharacter.lowercased() != stringCharacter{
                guard index != 0 else { continue } //"ILoveSwift" should not turn into " I Love Swift"
                let stringIndex = self.index(self.startIndex, offsetBy: index + indexOffset)
                let endStringIndex = self.index(self.startIndex, offsetBy: index + 1 + indexOffset)
                let range = stringIndex..<endStringIndex
                indexOffset += 1
                self.replaceSubrange(range, with: " \(stringCharacter)")
            }
        }
    }
}
