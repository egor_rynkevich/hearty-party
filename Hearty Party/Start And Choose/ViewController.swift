//
//  ViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/17/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import RealmSwift

protocol ViewControllerDelegate: class{
    func generateShopingList(numGuests: Int, holidayType: HolidayTypeEnum, nationArr: [DishNationEnum])
    func showNumberVC(holidayType: HolidayTypeEnum)
    func showThemeVC(holidayType: HolidayTypeEnum, numGuests: Int)
}

class ViewController: UIViewController, ViewControllerDelegate {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var newPartyBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        gradient.startPoint = CGPoint(x: 0.5, y: 0)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.7)
        gradient.colors = [UIColor(red: 244.0 / 255.0, green: 192.0 / 255.0, blue: 49.0 / 255.0, alpha: 1).cgColor as Any, UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor as Any]
        
        self.view.backgroundColor = UIColor.init(patternImage: UIImage().image(fromLayer: gradient))
        backView.backgroundColor = UIColor.init(patternImage: UIImage(named: "rectangle.png")!.resize(targetSize: CGSize(width: UIScreen.main.bounds.width, height: backView.frame.height)))
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        let imgArr = [UIImage(named: "01.png")!.resize(targetSize: CGSize(width: 230, height: 260)), UIImage(named: "02.png")!.resize(targetSize: CGSize(width: 230, height: 260)), UIImage(named: "03.png")!.resize(targetSize: CGSize(width: 230, height: 260)), UIImage(named: "04.png")!.resize(targetSize: CGSize(width: 230, height: 260))]
        let animImg = UIImage.animatedImage(with: imgArr, duration: 0.7)
        imgView.image = animImg
        imgView.startAnimating()
        imgView.contentMode = .scaleToFill
        //DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
          //  self.imgView.stopAnimating()
            //self.imgView.image = UIImage(named: "01.png")!.resize(targetSize: CGSize(width: 230, height: 260))
        //}
        
        newPartyBtn.setTitleColor(UIColor(red: 0.96, green: 0.76, blue: 0.07, alpha: 1), for: .normal)
        newPartyBtn.layer.borderWidth = 2
        newPartyBtn.layer.borderColor = UIColor(red: 0.96, green: 0.76, blue: 0.07, alpha: 1).cgColor
        newPartyBtn.layer.masksToBounds = true
        newPartyBtn.layer.cornerRadius = 27.5
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if let cntlr = storyboard.instantiateViewController(withIdentifier: "CreateShopingListVC") as? CreateShopingListVC {
//            //let myIndexPath = self.tableNode.indexPathForSelectedRow!
//            cntlr.holidayType = .Picnic
//            cntlr.numGuests =  8
//            cntlr.nationArr = []
//            self.navigationController?.pushViewController(cntlr, animated: true)
//        }
    }

    @IBAction func newOcassionPressBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "OcassionTableViewController") as? OcassionTableViewController {
            cntlr.delegate = self
            self.present(cntlr, animated: true) {
                //self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    func showNumberVC(holidayType: HolidayTypeEnum) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "NumberThemeViewController") as? NumberThemeViewController {
            cntlr.holidayType = holidayType
            cntlr.delegate = self
            self.present(cntlr, animated: true) {
            }
        }
    }
    
    func showThemeVC(holidayType: HolidayTypeEnum, numGuests: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "ChooseThemeVC") as? ChooseThemeVC {
            cntlr.holidayType = holidayType
            cntlr.delegate = self
            cntlr.numGuests = numGuests
            self.present(cntlr, animated: true) {
            }
        }
    }
    
    func generateShopingList(numGuests: Int, holidayType: HolidayTypeEnum, nationArr: [DishNationEnum]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "CreateShopingListVC") as? CreateShopingListVC {
            //let myIndexPath = self.tableNode.indexPathForSelectedRow!
            cntlr.holidayType = holidayType
            cntlr.numGuests =  numGuests
            cntlr.nationArr = nationArr
            self.navigationController?.pushViewController(cntlr, animated: true)
        }
    }
}

extension UIImage {
    
    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
    
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
    
}
