//
//  NumberThemeViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/21/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class NumberThemeViewController: UIViewController {
 
    @IBOutlet weak var generateBtn: UIButton!
    @IBOutlet weak var numberPickerView: UIPickerView!
    @IBOutlet weak var numberLbl: UILabel!
    
    var numGuests: Int = 1
    var holidayType: HolidayTypeEnum = .BBQ
    var canGenerateMenu: Bool = false
    var delegate: ViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberPickerView.delegate = self
        numberPickerView.dataSource = self
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        gradient.startPoint = CGPoint(x: 0.5, y: 0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1)
        gradient.colors = [UIColor(red: 244.0 / 255.0, green: 192.0 / 255.0, blue: 49.0 / 255.0, alpha: 1).cgColor as Any, UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor as Any]
        
        self.view.backgroundColor = UIColor.init(patternImage: UIImage().image(fromLayer: gradient))
        
        numberLbl.textColor = UIColor(red: 245.0 / 255.0, green: 192.0 / 255.0, blue: 19.0 / 255.0, alpha: 1)
        numberLbl.layer.cornerRadius = 135.0 / 2
        numberLbl.layer.masksToBounds = true
        
        generateBtn.addTarget(self, action: #selector(showThemeVC), for: .touchUpInside)
        generateBtn.layer.cornerRadius = 27
        generateBtn.layer.borderWidth = 2
        generateBtn.layer.borderColor = UIColor.white.cgColor
        generateBtn.setTitleColor(UIColor.white, for: .normal)
        
        print(numberPickerView.subviews.count)
    }
    
    @objc func showThemeVC() {
        //let p = self.presentingViewController as? OcassionTableViewController
        self.dismiss(animated: true, completion: nil)
        self.delegate?.showThemeVC(holidayType: self.holidayType, numGuests: self.numGuests)
        dismiss(animated: true) {
            self.delegate?.showThemeVC(holidayType: self.holidayType, numGuests: self.numGuests)
            //p?.dismiss(animated: true, completion: nil)
        }
    }
}



extension NumberThemeViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 50
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row+1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let myTitle = NSAttributedString(string: "\(row+1)", attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedString.Key.foregroundColor:UIColor.white])
        return myTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        numGuests = row + 1
        numberLbl.text = "\(numGuests)"
    }
}
