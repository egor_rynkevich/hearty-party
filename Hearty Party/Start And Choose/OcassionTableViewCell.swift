//
//  OcassionTableViewCell.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/23/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit

class OcassionTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(img: UIImage?, title: String) {
        imgView.image = img
        titleLbl.text = title
        titleLbl.textColor = UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1)
        backgroundColor = nil
        
        cellView.layer.cornerRadius = 4
        cellView.layer.borderWidth = 1.0
        cellView.layer.borderColor = UIColor.clear.cgColor
        cellView.layer.masksToBounds = true
        
        cellView.layer.shadowColor = UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor
        cellView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cellView.layer.shadowRadius = 2.0
        cellView.layer.shadowOpacity = 0.5
        cellView.layer.masksToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
