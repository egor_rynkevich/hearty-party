//
//  IngredientListDN.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/28/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class IngredientListDN: ASDisplayNode {
    var tableNode: ASTableNode = ASTableNode(style: .plain)
    var ingredientsDict: [IngredientGroup: [IngredForShow]] = [:]
    var groupArr: [IngredientGroup] = []
    
    convenience init(ingDict: [IngredientGroup: [IngredForShow]], grpArr: [IngredientGroup]) {
        self.init()
        ingredientsDict = ingDict
        groupArr = grpArr
        
        tableNode.dataSource = self
        tableNode.delegate = self
        tableNode.view.isScrollEnabled = false
        tableNode.view.bounces = true
        tableNode.view.separatorStyle = .none
        //tableNode.backgroundColor = UIColor.red
        tableNode.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30000)
        tableNode.reloadData()
        tableNode.waitUntilAllUpdatesAreProcessed()
        addSubnode(tableNode)
        
    }
    
    override func nodeDidLoad() {
        super.nodeDidLoad()
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: tableNode)
    }
}


extension IngredientListDN: ASTableDelegate, ASTableDataSource {
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return ingredientsDict.count
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return ingredientsDict[groupArr[section]]!.count
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        return {
            let node = IngredientListNode(ingInfo: self.ingredientsDict[self.groupArr[indexPath.section]]![indexPath.row])
            return node
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeight: CGFloat = 30
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeight))
        view.backgroundColor = nil
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        
        var text = "\(groupArr[section])"
        text.seperatedWithSpaces()
        text = text.lowercased()
        text.capitalizeFirstLetter()
        
        let titleTextNode = ASTextNode()
        titleTextNode.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1), .foregroundColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1)])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.maximumNumberOfLines = 1
        titleTextNode.frame = CGRect(x: 13, y: 5, width: UIScreen.main.bounds.width - 30, height: 20)
        
        view.addSubnode(titleTextNode)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        
        let cell = tableNode.nodeForRow(at: indexPath) as? IngredientListNode
        cell?.changeCellState()
    }
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }
}


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
