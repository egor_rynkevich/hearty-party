//
//  IngredientListNode.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/28/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class IngredientListNode: ASCellNode {
    fileprivate let titleTextNode: ASTextNode
    fileprivate let imageNode: ASImageNode
    var isSelect = false
    var title = ""
    
    init(ingInfo: IngredForShow) {
        titleTextNode = ASTextNode()
        imageNode = ASImageNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        let backView = UIView()
        backView.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
        selectedBackgroundView = backView
        
        var amountTypeStr = ""
        
        if ingInfo.AmountType == IngredientCountTypeEnum.Abstract.rawValue || ingInfo.AmountType == IngredientCountTypeEnum.Whole.rawValue { amountTypeStr = "" }
        else {
            for type in IngredientCountTypeEnum.allCases {
                if type.rawValue == ingInfo.AmountType {
                    amountTypeStr = "\(type)"
                    amountTypeStr.seperatedWithSpaces()
                    amountTypeStr = amountTypeStr.lowercased()
                    break
                }
            }
        }
        var str = ingInfo.IngredientStrTemplate
        var amountStr = ""
        
        if ingInfo.IgredientAmount < 1 {
            if ingInfo.IgredientAmount == 0 || ingInfo.IgredientAmount == -1 { amountStr = "" }
            else if ingInfo.IgredientAmount == 0.25 { amountStr = "¼" }
            else if ingInfo.IgredientAmount == 0.5 { amountStr = "½" }
            else { amountStr = "¾" }
        } else {
            amountStr = "\(Int(ingInfo.IgredientAmount))"
        }
        
        var res = ""
        var isFindTwo = false
        for ch in str.characters {
            if isFindTwo {
                res.append(ch)
                break
            }
            else if ch == "2" {
                res.append(ch)
                isFindTwo = true
            }
            else {
                res.append(ch)
            }
        }
        str = res
        str = str.replacingOccurrences(of: "{0}", with: amountStr)
        str = str.replacingOccurrences(of: "{1}", with: amountTypeStr)
        str = str.replacingOccurrences(of: "{2}", with: ingInfo.Name)
        
        let range = (str as NSString).range(of: ingInfo.Name)
        
        title = str
        
        let attributedString = NSMutableAttributedString(string: str, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black , range: range)
        
        titleTextNode.attributedText = attributedString
        titleTextNode.style.maxWidth = ASDimension(unit: .points, value: UIScreen.main.bounds.width - 70)
        imageNode.image = UIImage(named: "unselect")?.resize(targetSize: CGSize(width: 22, height: 22))
        imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1))
        addSubnode(titleTextNode)
        addSubnode(imageNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        let stack = ASStackLayoutSpec(direction: .horizontal, spacing: 12, justifyContent: .start, alignItems: .center, children: [imageNode, titleTextNode])
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 11, left: 16, bottom: 11, right: 15), child: stack)
    }
    
    func changeCellState() {
        var color: UIColor?
        var textColor: UIColor?
        var imageName = ""
        
        if isSelect {
            textColor = UIColor.black
            color = UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1)
            imageName = "unselect"
            isSelect = false
        } else {
            textColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            color = UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 0.4)
            imageName = "select"
            isSelect = true
        }
        let attributedString = NSMutableAttributedString(string: title, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: textColor!])
        
        titleTextNode.attributedText = attributedString
        
        imageNode.image = UIImage(named: imageName)?.resize(targetSize: CGSize(width: 22, height: 22))
        imageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(color!)
    }
}
