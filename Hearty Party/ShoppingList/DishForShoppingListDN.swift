//
//  DishForShoppingListDN.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/28/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class DishForShoppingListDN: ASDisplayNode {
    var tableNode: ASTableNode = ASTableNode(style: .plain)
    var dishesDictForShow: [DishTypeEnum: [Dish]] = [:]
    var dishTypeArr: [DishTypeEnum] = []
    
    convenience init(dishesDict: [DishTypeEnum: [Dish]], dishArr: [DishTypeEnum]) {
        self.init()
        dishesDictForShow = dishesDict
        dishTypeArr = dishArr
        
        tableNode.dataSource = self
        tableNode.delegate = self
        tableNode.view.isScrollEnabled = false
        tableNode.view.bounces = true
        tableNode.view.separatorStyle = .none
        tableNode.backgroundColor = UIColor.clear
        //tableNode.backgroundColor = UIColor.red
        tableNode.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30000)
        tableNode.reloadData()
        tableNode.waitUntilAllUpdatesAreProcessed()
        addSubnode(tableNode)
        
    }
}

extension DishForShoppingListDN: ASTableDelegate, ASTableDataSource {
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return dishesDictForShow.count
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return (dishesDictForShow[dishTypeArr[section]]?.count ?? 0)
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        let dishRef: ThreadSafeReference<Dish> = ThreadSafeReference(to: dishesDictForShow[dishTypeArr[indexPath.section]]![indexPath.row])
        return {
            let node = DishCellNode(dishRef: dishRef, contentWidth: Int(UIScreen.main.bounds.size.width - 30))
            
            return node
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeight: CGFloat = 16
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeight))
        view.backgroundColor = nil
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        
        var text = "\(dishTypeArr[section])"
        text.seperatedWithSpaces()
        
        let titleTextNode = ASTextNode()
        titleTextNode.attributedText = NSAttributedString(string: text.uppercased(), attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1), .foregroundColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1)])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.maximumNumberOfLines = 1
        titleTextNode.frame = CGRect(x: 13, y: 0, width: UIScreen.main.bounds.width - 30, height: 16)
        
        view.addSubnode(titleTextNode)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
    }
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }
}
