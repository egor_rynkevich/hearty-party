//
//  ShopingListViewController.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/28/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit

struct IngredForShow {
    var Name: String = ""
    var AmountType: Int = 0
    var IgredientAmount: Float = 0.0
    var IngredientStrTemplate: String = ""
}

class ShopingListViewController: UIViewController {

    var scrollNode: ASScrollNode = ASScrollNode()

    var dishesDict: [DishTypeEnum: [Dish]] = [:]
    var dishTypeArr: [DishTypeEnum] = []
    var ingredientsDict: [IngredientGroup: [IngredForShow]] = [:]
    var peopleCount: Int = 0
    var holidayType: HolidayTypeEnum = .BBQ
    var nationArr: [DishNationEnum] = []
    var groupArr: [IngredientGroup] = []
    var ingDN: IngredientListDN?
    var dishDN: DishForShoppingListDN?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        title = "\(holidayType)"
        
        for (_, value) in dishesDict {
            for dish in value {
                for ingInf in dish.IngredientInfos {
                    
                    var type = IngredientGroup.Other
                    
                    if ingInf.ParentIngredient!.GroupId & IngredientGroup.BiscuitsSnacksAndSweets.rawValue == IngredientGroup.BiscuitsSnacksAndSweets.rawValue {
                        type = .BiscuitsSnacksAndSweets
                    } else if ingInf.ParentIngredient!.GroupId & IngredientGroup.CookingIngredients.rawValue == IngredientGroup.CookingIngredients.rawValue  {
                        type = .CookingIngredients
                    } else if ingInf.ParentIngredient!.GroupId & IngredientGroup.DairyEggsAndChilled.rawValue == IngredientGroup.DairyEggsAndChilled.rawValue  {
                        type = .DairyEggsAndChilled
                    } else if ingInf.ParentIngredient!.GroupId & IngredientGroup.FruitAndVegetables.rawValue == IngredientGroup.FruitAndVegetables.rawValue  {
                        type = .FruitAndVegetables
                    } else if ingInf.ParentIngredient!.GroupId & IngredientGroup.MeatFishAndPoultry.rawValue == IngredientGroup.MeatFishAndPoultry.rawValue  {
                        type = .MeatFishAndPoultry
                    } else if ingInf.ParentIngredient!.GroupId & IngredientGroup.TinsPacketsAndJars.rawValue == IngredientGroup.TinsPacketsAndJars.rawValue  {
                        type = .TinsPacketsAndJars
                    }
                    
                    if !groupArr.contains(type) {
                        groupArr.append(type)
                    }
                    
                    let res = ingInf.IgredientAmount / Float(ingInf.ParentDish!.Serves) * Float(peopleCount)
                    
                    let ing = IngredForShow(Name: ingInf.ParentIngredient!.Name!, AmountType: ingInf.AmountType, IgredientAmount: res, IngredientStrTemplate: ingInf.IngredientStrTemplate!)
                    
                    if let _ = ingredientsDict[type] {
                        if let index = ingredientsDict[type]!.firstIndex(where: { (i) -> Bool in
                            return i.Name == ing.Name && i.AmountType == ing.AmountType
                        }) {
                            ingredientsDict[type]![index].IgredientAmount += ing.IgredientAmount
                        } else {
                            ingredientsDict[type]?.append(ing)
                        }
                        
                        
                    }
                    else { ingredientsDict[type] = [ing] }
                }
            }
        }
        
        scrollNode.automaticallyManagesSubnodes = true
        scrollNode.automaticallyManagesContentSize = true
        scrollNode.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        

        ingDN = IngredientListDN(ingDict: ingredientsDict, grpArr: groupArr)
        dishDN = DishForShoppingListDN(dishesDict: dishesDict, dishArr: dishTypeArr)

        var size = ingDN?.tableNode.view.contentSize
        size?.height += 10
        ingDN?.style.preferredSize = size!
        
        var size2 = dishDN?.tableNode.view.contentSize
        dishDN?.style.preferredSize = size2!
        
        scrollNode.layoutSpecBlock = { node, constrainedSize in
            let stack = ASStackLayoutSpec.vertical()

            let titleTextNode = ASTextNode()
            titleTextNode.attributedText = NSAttributedString(string: "Shopping list", attributes: [.font : UIFont.systemFont(ofSize: 20), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            titleTextNode.frame = CGRect(x: 16, y: 15, width: 200, height: 50)
            let textOfSize: CGFloat = 17

            let peopleImageNode = ASImageNode()
            peopleImageNode.image = UIImage(named: "people.png")?.resize(targetSize: CGSize(width: 22, height: 16))
            peopleImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1))
            peopleImageNode.frame = CGRect(x: 140, y: 20, width: 22, height: 16)
            let numberTextNode = ASTextNode()
            numberTextNode.attributedText = NSAttributedString(string: "(\(self.peopleCount))", attributes: [.font : UIFont.systemFont(ofSize: textOfSize), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 1)])
            numberTextNode.frame = CGRect(x: 168, y: 17, width: 60, height: 30)
            //let peopleStck = ASStackLayoutSpec(direction: .horizontal, spacing: 6, justifyContent: .start, alignItems: .center, children: [titleTextNode, peopleImageNode, numberTextNode])
            
            let arrowImageNode = ASImageNode()
            arrowImageNode.image = UIImage(named: "top_arrow")?.resize(targetSize: CGSize(width: 13, height: 8))
            arrowImageNode.imageModificationBlock = ASImageNodeTintColorModificationBlock(UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1))
            arrowImageNode.frame = CGRect(x: UIScreen.main.bounds.width - 30, y: 23, width: 13 , height: 8)
            
            let dn = ASDisplayNode()
            dn.addSubnode(titleTextNode)
            dn.addSubnode(peopleImageNode)
            dn.addSubnode(numberTextNode)
            dn.addSubnode(arrowImageNode)
            //dn.addSubnode(self.ingDN!)
            dn.backgroundColor = UIColor.white
            //size?.height += 40
            dn.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 50)
            

            let line1 = ASDisplayNode()
            line1.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line1.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 22, left: 0, bottom: 0, right: 0), child: line1))
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: dn))

            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: self.ingDN!))
            
            let line2 = ASDisplayNode()
            line2.style.preferredSize = CGSize(width: UIScreen.main.bounds.width, height: 1)
            line2.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child: line2))
            
            let btnNode = ASButtonNode()
            btnNode.setAttributedTitle(NSAttributedString(string: "BUY NEAR ME", attributes: [.font : UIFont.boldSystemFont(ofSize: 21), NSAttributedString.Key.strokeColor: UIColor.white, .foregroundColor: UIColor(red: 0.96, green: 0.76, blue: 0.07, alpha: 1)]), for: .normal)
            btnNode.style.preferredSize = CGSize(width: 232, height: 55)
            btnNode.cornerRadius = 27.5
            btnNode.borderColor = UIColor(red: 0.96, green: 0.76, blue: 0.07, alpha: 1).cgColor
            btnNode.borderWidth = 2
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 22, left: 75, bottom: 0, right: 75), child: btnNode))
            
            stack.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 22, left: 0, bottom: 10, right: 0), child: self.dishDN!))
            
            return stack
        }

        self.view.addSubnode(scrollNode)
        
        let shareBar: UIBarButtonItem = UIBarButtonItem(title: "Finish", style: .plain, target: self, action: nil)
        shareBar.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        self.navigationItem.rightBarButtonItem = shareBar
        
        
        let back = UIBarButtonItem(image: UIImage(named: "pen")?.resize(targetSize: CGSize(width: 20, height: 20)), style: .plain, target: self, action: #selector(backBtn))
        back.tintColor = UIColor(red: 0.96, green: 0.56, blue: 0.2, alpha: 1)
        self.navigationItem.setLeftBarButton(back, animated: true)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func backBtn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.scrollNode.frame = self.view.bounds
        //self.ingDN!.frame = self.view.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
}


