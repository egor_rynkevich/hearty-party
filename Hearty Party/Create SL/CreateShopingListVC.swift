//
//  CreateShopingListVC.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/22/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class CreateShopingListVC: UIViewController {
    @IBOutlet weak var viewForTableNode: UIView!
    
    var numGuests: Int = 0
    var holidayType: HolidayTypeEnum = .BBQ
    var tableNode: ASTableNode = ASTableNode(style: .grouped)
    var nationArr: [DishNationEnum] = []
    var dishes: [Dish] = []
    var dietArr: [DietEnum] = []
    
    var dishesDict: [DishTypeEnum: [Dish]] = [:]
    var dishesDictForShow: [DishTypeEnum: [Dish]] = [:]
    var dishTypeArr: [DishTypeEnum] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        
        self.title = "\(holidayType)"
        
        let cancelBarButtomItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backBtn))
        cancelBarButtomItem.tintColor = UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1)
        self.navigationItem.setLeftBarButton(cancelBarButtomItem, animated: true)
        
        let settingsBarButtonItem = UIBarButtonItem(image: UIImage(named: "settings.png")?.resize(targetSize: CGSize(width: 25, height: 25)), style: .plain, target: self, action: #selector(filtersBtn))
        settingsBarButtonItem.tintColor = UIColor(red: 0.96, green: 0.58, blue: 0.2, alpha: 1)
        self.navigationItem.rightBarButtonItem = settingsBarButtonItem
        
        tableNode.view.separatorStyle = .none
        tableNode.dataSource = self
        tableNode.delegate = self
        tableNode.contentInset = UIEdgeInsets(top: 14, left: 0, bottom: 0, right: 0)
        
        self.viewForTableNode.addSubnode(tableNode)
        
        configData()
    }
    
    func configData() {
        dishesDict.removeAll()
        dishesDictForShow.removeAll()
        dishTypeArr.removeAll()
        
        let realm = try! Realm()
        
        let res = realm.objects(Dish.self)
        
        for dish in res {
            if dish.HolidayType & holidayType.rawValue == holidayType.rawValue && checkOnDiet(dish: dish) {
                
                dishes.append(dish)
                
                var type = DishTypeEnum.Appetizer
                
                if dish.DishType & DishTypeEnum.Appetizer.rawValue == DishTypeEnum.Appetizer.rawValue {
                    type = DishTypeEnum.Appetizer
                } else if dish.DishType & DishTypeEnum.Salad.rawValue == DishTypeEnum.Salad.rawValue {
                    type = DishTypeEnum.Salad
                } else if dish.DishType & DishTypeEnum.MainCourse.rawValue == DishTypeEnum.MainCourse.rawValue {
                    type = DishTypeEnum.MainCourse
                } else if dish.DishType & DishTypeEnum.Dessert.rawValue == DishTypeEnum.Dessert.rawValue {
                    type = DishTypeEnum.Dessert
                }
                
                if let _ = dishTypeArr.firstIndex(where: { $0 == type }) {
                } else { dishTypeArr.append(type) }
                
                //dishesDict[index]?.append(dish)
                if let _ = dishesDict[type] { dishesDict[type]?.append(dish) }
                else { dishesDict[type] = [dish] }
            }
        }
        
        dishTypeArr.sort { (t1, t2) -> Bool in
            return t1.rawValue < t2.rawValue
        }
        
        for (key, val) in dishesDict {
            for i in 0..<val.count {
                if i == 2 { break }
                if let _ = dishesDictForShow[key] { dishesDictForShow[key]?.append(val[i]) }
                else { dishesDictForShow[key] = [val[i]] }
            }
        }
        for key in dishTypeArr {
            for elem in self.dishesDictForShow[key]! {
                if let index = self.dishesDict[key]?.firstIndex(of: elem) {
                    self.dishesDict[key]?.append(elem)
                    self.dishesDict[key]?.remove(at: index)
                }
            }
        }
        
        
        tableNode.reloadData()
    }
    
    func checkOnDiet(dish: Dish) -> Bool {
        if dietArr.count == 0 {
            return true
        }
        
        for diet in dietArr {
            if dish.Diet & diet.rawValue == diet.rawValue {
                return true
            }
        }
        
        return false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print(self.viewForTableNode.bounds)
        print(UIScreen.main.bounds.width)
        print(UIScreen.main.bounds.height)

        self.tableNode.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 80)
        
    }
    
    @objc func backBtn() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func filtersBtn() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "FiltersViewController") as? FiltersViewController {
            //let myIndexPath = self.tableNode.indexPathForSelectedRow!
            cntlr.dishTypeArr = dishTypeArr
            cntlr.holidayType = holidayType
            cntlr.themeArr = nationArr
            cntlr.numGuests = numGuests
            cntlr.delegate = self
            self.navigationController?.pushViewController(cntlr, animated: true)
        }
    }
}

extension CreateShopingListVC: FiltersViewControllerDelegate {
    func setNewData(num: Int, diet: [DietEnum]) {
        numGuests = num
        dietArr = diet
        configData()
    }
}

extension CreateShopingListVC: ASTableDelegate, ASTableDataSource {
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return "\(dishTypeArr[section])"
//    }
    
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return dishesDictForShow.count
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        let needAddBtn: Int = (dishesDictForShow[dishTypeArr[section]]?.count ?? 0) < (dishesDict[dishTypeArr[section]]?.count ?? 0) ? 1 : 0
        let needCreateBtn: Int = (section + 1) == dishTypeArr.count ? 1 : 0
        
        return (dishesDictForShow[dishTypeArr[section]]?.count ?? 0) + needAddBtn + needCreateBtn
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        var dishRef: ThreadSafeReference<Dish>?
        
        var isNeedPlus = false
        var isNeedCreate = false
        
        if dishesDictForShow[dishTypeArr[indexPath.section]]!.count == indexPath.row && dishesDictForShow[dishTypeArr[indexPath.section]]!.count != dishesDict[dishTypeArr[indexPath.section]]!.count {
            isNeedPlus = true
        }
        else if (indexPath.section + 1) == dishTypeArr.count &&  (dishesDictForShow[dishTypeArr[indexPath.section]]!.count + 1) == indexPath.row {
            isNeedCreate = true
        }
        else {
            dishRef = ThreadSafeReference(to: dishesDictForShow[dishTypeArr[indexPath.section]]![indexPath.row])
        }
        
        return {
            var node: ASCellNode?
            if isNeedPlus { node = AddDishCellNode(section: indexPath.section)
                (node as? AddDishCellNode)?.delegate = self
            }
            else if isNeedCreate { node = CreateShopingListCellNode()
                (node as? CreateShopingListCellNode)?.delegate = self
            }
            else { node = DishCellNode(dishRef: dishRef!, contentWidth: Int(UIScreen.main.bounds.size.width - 30)) }
            
            return node!
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeight: CGFloat = 16
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: viewHeight))
        view.backgroundColor = nil
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        
        var text = "\(dishTypeArr[section])"
        text.seperatedWithSpaces()
        
        let titleTextNode = ASTextNode()
        titleTextNode.attributedText = NSAttributedString(string: text.uppercased(), attributes: [NSAttributedString.Key.paragraphStyle : paragraphStyle, .font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.strokeColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1), .foregroundColor: UIColor(red: 0.43, green: 0.43, blue: 0.43, alpha: 1)])
        titleTextNode.truncationMode = .byTruncatingTail
        titleTextNode.maximumNumberOfLines = 1
        titleTextNode.frame = CGRect(x: 13, y: 0, width: UIScreen.main.bounds.width - 30, height: 16)
        
        view.addSubnode(titleTextNode)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        tableNode.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "DishViewController") as? DishViewController {
            //let myIndexPath = self.tableNode.indexPathForSelectedRow!
            cntlr.dish = dishesDictForShow[dishTypeArr[indexPath.section]]![indexPath.row]
            cntlr.peopleCount = numGuests
            self.navigationController?.pushViewController(cntlr, animated: true)
        }
    }
    
    @objc(tableNode:constrainedSizeForRowAtIndexPath:)
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let min = CGSize(width: UIScreen.main.bounds.size.width, height: 1)
        let max = CGSize(width: UIScreen.main.bounds.size.width, height: CGFloat.greatestFiniteMagnitude)
        return ASSizeRange(min: min, max: max)
    }
}

extension CreateShopingListVC: AddDishAndCreateListCellNodeDelegate {
    func createShopingList() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let cntlr = storyboard.instantiateViewController(withIdentifier: "ShopingListViewController") as? ShopingListViewController {
            cntlr.peopleCount = numGuests
            cntlr.dishesDict = dishesDictForShow
            cntlr.holidayType = holidayType
            cntlr.nationArr = nationArr
            cntlr.dishTypeArr = dishTypeArr
            self.navigationController?.pushViewController(cntlr, animated: true)
        }
    }
    
    func addNewDish(section: Int) {
        
        for elem in dishesDict[dishTypeArr[section]]!
        {
            if !dishesDictForShow[dishTypeArr[section]]!.contains(elem) {
                dishesDictForShow[dishTypeArr[section]]?.append(elem)
                tableNode.insertRows(at: [IndexPath(row: dishesDictForShow[dishTypeArr[section]]!.count - 1, section: section)], with: .bottom)
                //tableNode.reloadData()
                break
            }
        }
        
        //insertRows(at: [IndexPath(row: dishesDictForShow[dishTypeArr[section]]!.count-1, section: section)], with: .bottom)
    }
}

extension CreateShopingListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let actionDelete = UIContextualAction(style: .normal, title: "") { (action, view, complition) in
            self.dishesDictForShow[self.dishTypeArr[indexPath.section]]?.remove(at: indexPath.row)
            self.tableNode.deleteRows(at: [indexPath], with: .top)
            self.tableNode.reloadData()
            complition(true)
        }
        actionDelete.image = UIImage(named: "trash.png")!.resize(targetSize: CGSize(width: 27, height: 30))
        actionDelete.backgroundColor = UIColor(red: 0.96, green: 0.19, blue: 0.24, alpha: 1)
        

        //let title = isNoSound == true ? "Unmute" : "Mute"
        
        let actionRefresh = UIContextualAction(style: .normal, title: "") { (action, view, complition) in
            for elem in self.dishesDictForShow[self.dishTypeArr[indexPath.section]]! {
                if let index = self.dishesDict[self.dishTypeArr[indexPath.section]]?.firstIndex(of: elem) {
                    self.dishesDict[self.dishTypeArr[indexPath.section]]?.append(elem)
                    self.dishesDict[self.dishTypeArr[indexPath.section]]?.remove(at: index)
                }
            }
            
            let dish = self.dishesDict[self.dishTypeArr[indexPath.section]]![0]
            
            if !self.dishesDictForShow[self.dishTypeArr[indexPath.section]]!.contains(dish) {
                self.dishesDictForShow[self.dishTypeArr[indexPath.section]]?.remove(at: indexPath.row)
                self.dishesDictForShow[self.dishTypeArr[indexPath.section]]?.insert(dish, at: indexPath.row)
                
                self.dishesDict[self.dishTypeArr[indexPath.section]]?.append(dish)
                self.dishesDict[self.dishTypeArr[indexPath.section]]?.remove(at: 0)
                
                self.tableNode.reloadRows(at: [indexPath], with: .right)
            }
            
            complition(true)
        }
//        let imgArr = [UIImage(named: "refresh1.png")!.resize(targetSize: CGSize(width: 30, height: 30)), UIImage(named: "refresh2.png")!.resize(targetSize: CGSize(width: 30, height: 30)), UIImage(named: "refresh3.png")!.resize(targetSize: CGSize(width: 30, height: 30)), UIImage(named: "refresh4.png")!.resize(targetSize: CGSize(width: 30, height: 30)), UIImage(named: "refresh5.png")!.resize(targetSize: CGSize(width: 30, height: 30)), UIImage(named: "refresh6.png")!.resize(targetSize: CGSize(width: 30, height: 30))]
//        let animImg = UIImage.animatedImage(with: imgArr, duration: 0.3)
        
        actionRefresh.image = UIImage(named: "refresh1.png")!.resize(targetSize: CGSize(width: 30, height: 30))
        
        
        actionRefresh.backgroundColor = UIColor(red: 0.96, green: 0.76, blue: 0.08, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [actionRefresh, actionDelete])
    }
}
