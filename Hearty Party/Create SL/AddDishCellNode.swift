//
//  AddDishCellNode.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/23/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit

protocol AddDishAndCreateListCellNodeDelegate: class{
    func addNewDish(section: Int)
    func createShopingList()
}

class AddDishCellNode: ASCellNode {
    
    fileprivate let btnNode: ASButtonNode
    var _section: Int = 0
    var delegate: AddDishAndCreateListCellNodeDelegate?
    
    init(section: Int) {
        btnNode = ASButtonNode()
        super.init()
        automaticallyManagesSubnodes = true
        let backView = UIView()
        backView.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
        selectedBackgroundView = backView
        _section = section
        btnNode.setImage(UIImage(named: "button_add.png")?.resize(targetSize: CGSize(width: 36, height: 36)), for: .normal)
        btnNode.style.preferredSize = CGSize(width: 36, height: 36)
        btnNode.addTarget(self, action: #selector(addDish), forControlEvents: .touchUpInside)
        addSubnode(btnNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0), child: ASCenterLayoutSpec(horizontalPosition: .center, verticalPosition: .center, sizingOption: .minimumHeight, child: btnNode))
    }
    
    @objc func addDish() {
        delegate?.addNewDish(section: _section)
    }
}
