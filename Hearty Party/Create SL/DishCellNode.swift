//
//  DishCellNode.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/23/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift

class DishCellNode: ASCellNode {
    
    fileprivate let titleTextNode: ASTextNode
    fileprivate let imageNode: ASNetworkImageNode
    fileprivate let backNode: ASImageNode
    fileprivate let veganImageNode: ASImageNode
    fileprivate let spicyImageNode: ASImageNode
    fileprivate let sepNode: ASImageNode

    var photo_url: String = ""
    var dish_id: Int = 0
    
    init(dishRef: ThreadSafeReference<Dish>, contentWidth: Int) {
        titleTextNode = ASTextNode()
        imageNode = ASNetworkImageNode()
        backNode = ASImageNode()
        veganImageNode = ASImageNode()
        spicyImageNode = ASImageNode()
        sepNode = ASImageNode()
        super.init()
        automaticallyManagesSubnodes = true
        
        let backView = UIView()
        backView.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
        selectedBackgroundView = backView
        
        let realm = try! Realm()
        
        if let dish = realm.resolve(dishRef) {
            photo_url = dish.PosterUrl ?? ""
            let imgHeight = photo_url == "" ? 0 : 70
            let imgWidth = contentWidth / 2
            
            titleTextNode.attributedText = NSAttributedString(string: dish.Name!, attributes: [.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.strokeColor: UIColor.black, .foregroundColor: UIColor.black])
            titleTextNode.truncationMode = .byTruncatingTail
            titleTextNode.backgroundColor = UIColor.clear
            titleTextNode.maximumNumberOfLines = 2
            titleTextNode.style.maxWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 15 - imgWidth))
            titleTextNode.style.minWidth = ASDimension(unit: .points, value: CGFloat(contentWidth - 15 - imgWidth))
            titleTextNode.style.minHeight = ASDimension(unit: .points, value: 35)
            
            imageNode.style.preferredSize = CGSize(width: imgWidth, height: imgHeight)
            
            if photo_url != "" {
                let data: Data? = nil
                
                if data == nil { imageNode.url = NSURL(string: photo_url)! as URL }
                else { imageNode.image = UIImage(data: data! as Data) }
                
                imageNode.imageModificationBlock = { image in
//                    if vkDataCache.getDataFromCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo") == nil {
//                        vkDataCache.saveDataInCachedData(sourceID: self?.photo_id ?? 0, ownerID: self?.owner_id ?? 0, type: "image", tag: "photo", url: self?.photo_url ?? "", image: image)
//                    }
                    return image
                }
                imageNode.cornerRadius = 4
            }
            
            veganImageNode.style.preferredSize = CGSize(width: 11, height: 20)
            //veganImageNode.contentMode =
            if dish.Diet & DietEnum.Vegetarian.rawValue == DietEnum.Vegetarian.rawValue {
                veganImageNode.image = UIImage(named: "vegan.png")?.resize(targetSize: CGSize(width: 11, height: 20))
            }
            
            
            spicyImageNode.style.preferredSize = CGSize(width: 11, height: 20)
            if dish.Diet & DietEnum.Spicy.rawValue == DietEnum.Spicy.rawValue {
                spicyImageNode.image = UIImage(named: "spicy")?.resize(targetSize: CGSize(width: 11, height: 20))
            }
            
            backNode.backgroundColor = UIColor.white
            backNode.cornerRadius = 4
            backNode.borderColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1).cgColor
            backNode.borderWidth = 1
            
            sepNode.backgroundColor = UIColor.clear
            sepNode.cornerRadius = 1
            sepNode.style.minWidth = ASDimension(unit: .points, value: 20)
            sepNode.style.height = ASDimension(unit: .points, value: 1)
            sepNode.style.flexGrow = 1
            sepNode.style.flexShrink = 1
            
            addSubnode(titleTextNode)
            addSubnode(imageNode)
        }
        
        
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let hStack = ASStackLayoutSpec(direction: .horizontal, spacing: 5, justifyContent: .center, alignItems: .center, children: [sepNode, spicyImageNode, veganImageNode])
        let vStack = ASStackLayoutSpec(direction: .vertical, spacing: 1, justifyContent: .start, alignItems: .stretch, children: [ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child: hStack), titleTextNode])
        
        let finalStack = ASStackLayoutSpec(direction: .horizontal, spacing: 9, justifyContent: .start, alignItems: .center, children: [imageNode, vStack])
        let back = ASBackgroundLayoutSpec(child: finalStack, background: backNode)
        let finalInsetSpec = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 10, bottom: 15, right: 15), child: back)
        return finalInsetSpec
    }
    
    override func didLoad() {
        imageNode.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMinXMaxYCorner]
    }
}
