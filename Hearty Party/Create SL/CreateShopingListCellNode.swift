//
//  CreateShopingListCellNode.swift
//  Hearty Party
//
//  Created by Егор Рынкевич on 1/27/19.
//  Copyright © 2019 Егор Рынкевич. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class CreateShopingListCellNode: ASCellNode {
    
    fileprivate let btnNode: ASButtonNode
    var delegate: AddDishAndCreateListCellNodeDelegate?
    
    override init() {
        btnNode = ASButtonNode()
        super.init()
        let backView = UIView()
        backView.backgroundColor = UIColor(red: 1, green: 0.98, blue: 0.81, alpha: 1)
        selectedBackgroundView = backView
        automaticallyManagesSubnodes = true
        btnNode.setAttributedTitle(NSAttributedString(string: "CREATE SHOPPING LIST", attributes: [.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.strokeColor: UIColor.white, .foregroundColor: UIColor.white]), for: .normal)
        btnNode.addTarget(self, action: #selector(createShopList), forControlEvents: .touchUpInside)
        btnNode.style.preferredSize = CGSize(width: 344, height: 55)
        btnNode.cornerRadius = 27.5
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: 344, height: 55)
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
        gradient.colors = [UIColor(red: 244.0 / 255.0, green: 192.0 / 255.0, blue: 49.0 / 255.0, alpha: 1).cgColor as Any, UIColor(red: 244.0 / 255.0, green: 144.0 / 255.0, blue: 50.0 / 255.0, alpha: 1).cgColor as Any]
        
        self.btnNode.backgroundColor = UIColor.init(patternImage: UIImage().image(fromLayer: gradient))
        
        
        addSubnode(btnNode)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0), child: ASCenterLayoutSpec(horizontalPosition: .center, verticalPosition: .center, sizingOption: .minimumHeight, child: btnNode))
    }
    
    @objc func createShopList() {
        delegate?.createShopingList()
    }
}
